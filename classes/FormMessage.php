<?
namespace Database\FormMessage;

class FormMessage
{
	protected $db;

	function __construct($database)
	{
		$this->db = $database;
	}

	public function write($data)
	{
		$this->db->query ('INSERT INTO messages', [
				'email' => $data['email'],
				'phone' => $data['phone'],
				'comment' => $data['comment']
			]);

		return true;
	}
}
