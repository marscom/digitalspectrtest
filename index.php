<?
use Database\FormMessage\FormMessage;

require_once ('vendor/autoload.php');
require_once ('classes/FormMessage.php');

$app = new \Slim\App();

$app->any('/sendMessage', function ($request, $response) {
	if ($request->isPost()) {
		$body = $request->getParsedBody();
		
		if ($body['email'] && $body['phone']) {

			$arValidatePhone = [
	            "options" => [
	                "regexp" => "/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/"
	            ]
	        ];

        	$phone = filter_var($body['phone'], FILTER_VALIDATE_REGEXP, $arValidatePhone);

        	if (!$phone) {
        		return $response->withJson([
		            'error' => 1,
		            'error_code' => 2,
		            'message' => 'Invalid phone number format'
		        ], 200);
        	}

        	$arValidateEmail = [
	            "options" => [
	                "regexp" => "/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i"
	            ]
	        ];

        	$email = filter_var($body['email'], FILTER_VALIDATE_REGEXP, $arValidateEmail);

        	if (!$email) {
        		return $response->withJson([
		            'error' => 1,
		            'error_code' => 3,
		            'message' => 'Invalid email format'
		        ], 200);
        	}

        	$comment = filter_var($body['comment']);

        	$arData = [
        		'email' => $email,
        		'phone' => $phone,
        		'comment' => $comment
        	];

        	$dbSettings = include('config.php');
			$database = new Nette\Database\Connection($dbSettings['dsn'], $dbSettings['user'], $dbSettings['password']);
        	$formMessage = new FormMessage($database);
			$arr = $formMessage->write($arData);

			return $response->withJson([
	            'error' => 0,
	            'message' => 'Success'
	        ], 200);
		} else {
			return $response->withJson([
	            'error' => 1,
	            'error_code' => 4,
	            'message' => 'Fields email and phone are required'
	        ], 200);
		}
	} else {
		return $response->withJson([
            'error' => 1,
            'error_code' => 1,
            'message' => 'Access denied'
        ], 200);
	}
});

$app->run();
